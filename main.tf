resource "aws_s3_bucket" "static_site" {
  bucket = "cdn-bucket-s3-static-site"

  tags = {
    Name  = "site in bucket"
    Owner = "katsiaryna"
  }
}


resource "aws_s3_bucket_public_access_block" "static_site" {
  bucket = aws_s3_bucket.static_site.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "static_site" {
  bucket = aws_s3_bucket.static_site.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}


resource "aws_s3_bucket_versioning" "static_site" {
  bucket = aws_s3_bucket.static_site.id
  versioning_configuration {
    status = "Enabled"
  }
}


resource "aws_s3_object" "content" {

  depends_on = [aws_s3_bucket_versioning.static_site]

  key                    = "index.html"
  bucket                 = aws_s3_bucket.static_site.id
  source                 = "index.html"
  server_side_encryption = "AES256"
  content_type           = "text/html"
}

resource "aws_cloudfront_origin_access_control" "static_site_access" {
  name                              = "security_cf_oac_s3_static_site"
  description                       = "security policy cdn s3"
  origin_access_control_origin_type = "s3"
  signing_behavior                  = "always"
  signing_protocol                  = "sigv4"
}


resource "aws_cloudfront_distribution" "static_site_access" {

  origin {
    domain_name              = aws_s3_bucket.static_site.bucket_domain_name
    origin_access_control_id = aws_cloudfront_origin_access_control.static_site_access.id
    origin_id                = aws_s3_bucket.static_site.id
  }

  depends_on = [
    aws_s3_bucket.static_site,
    aws_cloudfront_origin_access_control.static_site_access
  ]

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Distribution my static site"
  default_root_object = "index.html"


  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = aws_s3_bucket.static_site.id
    viewer_protocol_policy = "https-only"
    /*    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400 */

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }


  }

  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "whitelist"
      locations        = ["US", "CA", "GB", "DE"]
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

}

resource "aws_s3_bucket_policy" "s3_policy" {
  depends_on = [data.aws_iam_policy_document.site_policy]

  bucket = aws_s3_bucket.static_site.id
  policy = data.aws_iam_policy_document.site_policy.json
}


data "aws_iam_policy_document" "site_policy" {

  depends_on = [
    aws_s3_bucket.static_site,
    aws_cloudfront_distribution.static_site_access
  ]

  statement {
    sid     = "s3_bucket_cdn_static_site"
    effect  = "Allow"
    actions = ["s3:GetObject"]

    principals {
      type        = "Service"
      identifiers = ["cloudfront.amazonaws.com"]
    }

    resources = ["arn:aws:s3:::${aws_s3_bucket.static_site.id}/*"]

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceArn"

      /*    values = ["arn:aws:cloudfront::*:distribution/${aws_cloudfront_distribution.static_site_access.id}"]*/
      values = [aws_cloudfront_distribution.static_site_access.arn]

    }

  }
}


